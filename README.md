# stringlist

This module offers a drop-in `list`-like implementation specialized for
storing large amounts of strings and reducing memory overhead of stored string
objects while offering comparable performance to built-in lists.

I recommend using this package if you are working with large amounts of
small strings (e.g. the output of a tokenizer in an NLP pipeline) and
want to store them in memory but do not have enough memory to do so.
Note that it is often better to instead use batching to reduce the memory
usage of your application. However, if this is not an option or
causes too much of an overhead, this package may just be right for you.

## Installation

Simply use pip to install this package

```bash
pip install stringlist
```

or clone this repository and install it using the local files by running the
following command at the root of this repository

```bash
pip install .
```

## Usage

For most intents and purposes the string list can be used just like a normal
list with the exception that it can only store `str` or `str`-like objects.
Note that `str`-like objects will be converted to `str` when retrieved from the
list.

```python
from stringlist import StringList

sl = StringList()
sl.append("foo")
sl += ["bar"]

print(sl)  # ["foo", "bar"]

print(sl[0])  # "foo"

sl.reverse()
print(sl)  # ["bar", "foo"]
```

`StringList` even compares `True` when compared to built-in `list`s!

```python
l = list(["foo", "bar"])
sl = StringList(l)

print(l == sl)  # True
```

See also the [examples](examples/) directory.

## Documentation

Unfortunately, there is currently no official documentation.
Please refer either to the source code or the corresponding docstrings of
methods and classes.

## Benchmark

The following benchmark plots show the performance comparison of the
built-in `list` class and `StringList`.

Benchmarks have been performed for strings of length 10.

![Absolute Memory Benchmark](img/absolute-benchmark.svg)

![Relative Memory Benchmark](img/relative-benchmark.svg)

## Contributing

Contributions to improve this package are greatly appreciated. Feel free
to either create a pull request to get your changes merged or even
fork this repository to create your own version of it.

If you decide to create a pull request please ensure that the unit tests and
linter are green.
