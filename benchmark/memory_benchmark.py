import sys
from typing import Any, Callable, Dict, List, Tuple
import matplotlib.pyplot as plt

from stringlist import StringList


# Utility functions
def get_marker_generator():
    markers = ["o", "v", "^", "s", "p"]
    while True:
        for mark in markers:
            yield mark


def get_line_style_generator():
    line_styles = ["solid", "dashed", "dotted", "dashdot"]
    while True:
        for line_style in line_styles:
            yield line_style


def get_list_memory_usage(l: List[str]) -> int:
    # NOTE: "getsizeof" does NOT include the memory usage of contained elements
    # for "list" objects
    memory_usage = sys.getsizeof(l)
    memory_usage += sum(sys.getsizeof(s) for s in l)

    return memory_usage


def get_string_list_memory_usage(l: StringList) -> int:
    memory_usage = sys.getsizeof(l)
    memory_usage += sys.getsizeof(l.offsets)
    memory_usage += sys.getsizeof(l.strings)
    memory_usage += sys.getsizeof(l.size_type)
    memory_usage += sys.getsizeof(l.size_type_size)

    return memory_usage


def generate_string(length: int):
    # NOTE: It is important to generate a new string for every element in the
    # the list instead of reusign a single string. Since in that case the list
    # will simply store multiple references to the same string
    return "0" * length


# Benchmark function
def benchmark(
    start: int,
    stop: int,
    base: int,
    string_length: int,
    list_class: type,
    get_memory_usage: Callable[[Any], int]
) -> List[Tuple[int, int]]:
    results = []
    for i in range(start, stop):
        num_strings = base ** i

        # Construct the list
        l = list_class(generate_string(string_length) for _ in range(num_strings))
        
        # Measure the size of the list
        m = get_memory_usage(l)

        results.append((num_strings, m))

    return results


# Plotting functions
def plot_absolute_results(results: Dict[str, Tuple[int, int]], save_plot: bool = False):
    # Plot the results
    marker_generator = get_marker_generator()
    line_style_generator = get_line_style_generator()
    for benchmark_name, result in results.items():
        plt.plot(
            [r[0] for r in result],
            [r[1] for r in result],
            label=benchmark_name,
            marker=next(marker_generator),
            linestyle=next(line_style_generator)
        )

    plt.grid(True)
    plt.title("Absolute memory usage")
    plt.legend(loc="upper left")
    plt.xlabel("#strings")
    plt.ylabel("Memory usage (in bytes)")
    plt.xscale("log", base=10)
    plt.yscale("log", base=10)

    if save_plot:
        plt.savefig(f"img/absolute-benchmark.svg", format="svg")

    plt.show()


def plot_relative_results(results: Dict[str, Tuple[int, int]], save_plot: bool = False):
    marker_generator = get_marker_generator()
    line_style_generator = get_line_style_generator()

    list_results = results["list"]
    string_list_results = results["StringList"]
    zipped_results = list(zip(list_results, string_list_results))

    plt.plot(
        [r[0][0] for r in zipped_results],
        [r[1][1] / r[0][1] for r in zipped_results],
        marker=next(marker_generator),
        linestyle=next(line_style_generator)
    )
    plt.grid(True)
    plt.title("Relative memory usage")
    plt.xlabel("#strings")
    plt.ylabel("Relative memory usage")
    plt.xscale("log", base=10)

    if save_plot:
        plt.savefig(f"img/relative-benchmark.svg", format="svg")

    plt.show()

def main():
    results = dict()
    results["list"] = benchmark(0, 5, 10, 10, list, get_list_memory_usage)
    results["StringList"] = benchmark(0, 5, 10, 10, StringList, get_string_list_memory_usage)

    plot_absolute_results(results, save_plot=True)
    plot_relative_results(results, save_plot=True)


if __name__ == "__main__":
    main()
