from timeit import timeit

from stringlist.string_list import StringList

LIST = list(["0123456789" for _ in range(1, 100000)])
STRING_LIST = StringList(LIST)


def test1():
    LIST[0]
    


def test2():
    STRING_LIST[0]


a = timeit(test1, number=10)
b = timeit(test2, number=10)
print(a)
print(b)
