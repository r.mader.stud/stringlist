from distutils.core import setup
from Cython.Build import cythonize

REQUIREMENTS = [
    "cython==3.0a11"
]
DEV_REQUIREMENTS = [
    "pytest",
    "flake8"
]

setup(
    name="string-list",
    version="0.1",
    description=(
        "Drop-in replacement list specialized for storing strings with"
        "minimal memory overhead"
    ),
    author="Robin Mader",
    author_email="robdomader@aol.de",
    ext_modules=cythonize("stringlist/cython_ext/*"),
    install_requires=REQUIREMENTS,
    extras_require={
        "dev": DEV_REQUIREMENTS
    },
    packages=["stringlist"],
    zip_safe=False
)
