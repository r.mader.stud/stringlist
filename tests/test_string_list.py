from typing import List

import pytest
from stringlist import StringList

def test_empty_constructor():
    StringList()


@pytest.mark.parametrize("expected_strings", [
    ([]),
    (["foo", "apple"]),
    (["a", "b", "c"])
])
def test_constructor(expected_strings: List[str]):
    StringList(["foo", "apple"])


@pytest.mark.parametrize("expected_strings", [
    ([]),
    (["foo", "apple"]),
    (["a", "b", "c"])
])
def test_getitem_index(expected_strings: List[str]):
    string_list = StringList(expected_strings)

    for expected_string, string in zip(expected_strings, string_list):
        assert expected_string == string


@pytest.mark.parametrize("expected_strings", [
    ([]),
    (["foo", "apple"]),
    (["a", "b", "c"])
])
def test_getitem_slice_start_stop(expected_strings: List[str]):
    string_list = StringList(expected_strings)

    for i in range(0, len(expected_strings)):
        for j in range(i, len(expected_strings)):
            for expected_string, string in zip(expected_strings[i:j], string_list[i:j]):
                assert expected_string == string


@pytest.mark.parametrize("expected_strings", [
    ([]),
    (["foo", "apple"]),
    (["a", "b", "c"])
])
def test_len(expected_strings: List[str]):
    string_list = StringList(expected_strings)

    assert len(expected_strings) == len(string_list)


@pytest.mark.parametrize("expected_strings", [
    ([]),
    (["foo", "apple"]),
    (["a", "b", "c"])
])
def test_iter(expected_strings: List[str]):
    string_list = StringList(expected_strings)

    for string, expected_string in zip(string_list, expected_strings):
        assert string == expected_string


@pytest.mark.parametrize("expected_strings", [
    ([]),
    (["foo", "apple"]),
    (["a", "b", "c"])
])
def test_append(expected_strings: List[str]):
    string_list = StringList()
    expected_list = list()

    for string in expected_strings:
        string_list.append(string)
        expected_list.append(string)

        assert len(expected_list) == len(string_list)
        for string, expected_string in zip(string_list, expected_list):
            assert string == expected_string


@pytest.mark.parametrize("expected_strings", [
    ([]),
    (["foo", "apple"]),
    (["a", "b", "c"])
])
def test_copy(expected_strings: List[str]):
    string_list = StringList(expected_strings)
    string_list_copy = string_list.copy()

    assert string_list == string_list_copy
    for string, expected_string in zip(string_list, string_list_copy):
        assert string == expected_string
