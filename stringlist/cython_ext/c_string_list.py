# distutils: language=c++

import cython
from cython.cimports.libcpp.vector import vector
from cython.cimports.libcpp.string import string
from cython.cimports.libcpp.algorithm import find, reverse

from typing import Iterable, Callable, Any


@cython.cclass
class CStringList:
    """
    Thin wrapper for std::vector<string>.
    """
    
    _strings: vector[string]
    
    def __init__(self, strings: Iterable[str] = None):
        # Add initial strings (if any)
        if strings is not None:
            self._strings.reserve(len(strings))
            
            for s in strings:
                CStringList.append(self, s)

    def __getitem__(self, index: cython.Py_ssize_t):
        return self._strings.at(index)
    
    def __len__(self) -> int:
        return self._strings.size()
        
    def append(self, value: string):
        """Append the value to the end of the internal vector."""
        self._strings.push_back(value)

    def clear(self):
        """Remove all elements in this list."""
        self._strings.clear()
    
    def insert(self, index: cython.int, value: string):
        """
        Insert `value` at `index` in the list.
        
        WARNING: Inserting elements near the beginning of the list may be
        very slow.
        """
        self._strings.insert(self._strings.begin() + index, value)
    
    def pop(self, index: int = None) -> string:
        """
        Remove and return an element from the list.
        
        WARNING: Removing elements which are not near the end of the list may
        be slow.
        """
        if index is None:
            index = self._strings.size() - 1
        
        value: string = self._strings.at(index)
        self._strings.erase(
            self._strings.begin() + cython.cast(cython.int, index)
        )
            
        return value

    def remove(self, element: string):
        """Remove the first occurence of `element` from the list."""
        it = find(self._strings.begin(), self._strings.end(), element)
        
        if it == self._strings.end():
            raise ValueError(f"{element} not in list.")
        
        self._strings.erase(it)

    def reverse(self):
        """Reverse the order of all elements in the list."""
        reverse(self._strings.begin(), self._strings.end())

    def sort(self, key: Callable[[string], Any], reverse: bool = False):
        """Sort the list in ascending order."""
        # NOTE: This is probably not the optimal way to do this, since
        # it creates a second list and converts that to a new vector, but
        # it definitly is the easiest way to do this.
        self._strings: vector[string] = sorted(
            self._strings, key=key, reverse=reverse
        )
