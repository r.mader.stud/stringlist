from typing import Any, Callable, Iterable, Optional, Union, overload

from stringlist.cython_ext.c_string_list import CStringList


class StringList(CStringList):
    """
    Specialized container for storing strings.
    
    May contain empty strings but not `None`.
    """
    def __init__(self, strings: Iterable[str], encoding: str = "utf8"):
        self.encoding = encoding
        
        byte_strings = None
        if len(strings) > 0:
            byte_strings = [s.encode(self.encoding) for s in strings]

        super().__init__(byte_strings)

    def __eq__(self, other: Any):
        # NOTE: For compatibility reasons this will compare True when compared
        # to a list
        is_equal = False
        if isinstance(other, (list, StringList)):
            # Ensure that:
            # - The lists have the same length
            # - The lists have equal elements
            is_equal = len(self) == len(other) and \
                all(s1 == s2 for s1, s2 in zip(self, other))

        return is_equal
        
    @overload
    def __getitem__(self, index: int) -> str:
        ...

    @overload
    def __getitem__(self, keys: slice) -> "StringList":
        ...

    def __getitem__(self, keys: Union[int, slice]):
        if isinstance(keys, slice):
            # TODO
            pass
        else:
            index: int = keys  # rename for clarity
            byte_string = super().__getitem__(index)
            return byte_string.decode(self.encoding)

    def __iter__(self) -> Iterable[str]:
        for i in range(len(self)):
            yield self[i]
            
    def append(self, string: str):
        """Append the string to the end of the list."""
        byte_string = string.encode(self.encoding)
        super().append(byte_string)
        
    def copy(self) -> "StringList":
        """Create a deep copy of this list."""
        return StringList(self)
    
    def clear(self):
        """Remove all elements in this list."""
        super().clear()
        
    def count(self, value: str) -> int:
        """Get the sum of occurences of `value` in this list."""
        return sum(
            1 if s == value else 0
            for s
            in self
        )

    def extend(self, iterable: Iterable[str]):
        """Append all elements in `iterable` to this list."""
        for s in iterable:
            self.append(s)
    
    def index(self, value: str, start: Optional[int] = None, stop: Optional[int] = None) -> int:
        """Return the index of the first occurence of `value`."""
        start = start if start is not None else 0
        stop = stop if stop is not None else len(self)

        index = next([s for s in self[start:stop] if s == value], None)

        if index is None:
            raise ValueError(f"{repr(value)} is not in list.")

        return index

    def insert(self, index: int, value: str):
        """
        Insert `value` at `index` in the list.
        
        WARNING: Inserting elements near the beginning of the list may be
        very slow.
        """
        if index > len(self):
            self.append(value)
        else:
            # TODO
            pass
    
    def pop(self, index: int = None) -> str:
        """
        Remove and return an element from the list.
        
        WARNING: Removing elements which are not near the end of the list may
        be slow.
        """
        # TODO
        pass

    def remove(self, element: str):
        """Remove the first occurence of `element` from the list."""
        # TODO
        pass

    def reverse(self):
        """Reverse the order of all elements in the list."""
        # TODO
        pass

    def sort(self, key: Callable[[str], Any], reverse: bool):
        """Sort the list in ascending order."""
        # TODO
        pass
