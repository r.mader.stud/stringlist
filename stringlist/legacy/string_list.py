from typing import Any, Callable, Iterable, Optional, Union, overload
import struct
import array

class StringList:
    """
    Specialized container for storing strings.
    
    May contain empty strings but not `None`.
    """
    
    def __init__(
        self,
        strings: Iterable[str] = None,
        size_type: str = "i",
        encoding: str = "utf8"
    ):
        if strings is None:
            strings = []

        self.offsets = bytearray()
        self.strings = bytearray()
        self.size_type = size_type
        self.size_type_size = array.array(self.size_type).itemsize
        self.encoding = encoding

        for s in strings:
            self.append(s)

    def __eq__(self, other: Any):
        # NOTE: For compatibility reasons this will compare True when compared
        # to a list
        is_equal = False
        if isinstance(other, (list, StringList)):
            # Ensure that:
            # - The lists have the same length
            # - The lists have equal elements
            is_equal = len(self) == len(other) and \
                all(s1 == s2 for s1, s2 in zip(self, other))

        return is_equal

    @overload
    def __getitem__(self, index: int) -> str:
        ...

    @overload
    def __getitem__(self, keys: slice) -> "StringList":
        ...

    def __getitem__(self, keys: Union[int, slice]):
        if isinstance(keys, slice):
            return self._get_slice(keys.start, keys.stop, keys.step)
        else:
            index: int = keys  # rename for clarity
            return self._get_string(index)

    def __iter__(self) -> Iterable[str]:
        for i in range(len(self)):
            yield self[i]

    def __len__(self) -> int:
        return len(self.offsets) // self.size_type_size

    def _get_slice(self, start: int, stop: int, step: int) -> "StringList":
        # TODO: Creating a separate list and then using it to create a new
        # string list is probably not the most performanc way to go about this
        # in terms of memory and computing
        return StringList(list(self)[start:stop:step])

    def _get_string(self, index: int) -> str:
        """Get the string at position `index` in the list."""
        # Get position of string in internal buffer
        offset = self._get_offset(index)
        length = self._get_length(index)

        # Read string from buffer
        string = struct.unpack_from(f"{length}s", self.strings, offset)[0].decode(self.encoding)
        
        return string

    def _get_offset(self, index: int) -> int:
        """Get the offset of the string at position `index` in the list."""
        if index == 0:
            offset = 0
        else:
            offset_index = self.size_type_size * (index - 1)
            offset = struct.unpack_from(
                self.size_type, self.offsets, offset_index
            )[0]
        
        return offset

    def _set_offset(self, index: int, offset: int):
        if index == 0:
            return

        offset_index = self.size_type_size * (index - 1)
        struct.pack_into(self.size_type, self.offsets, offset_index, offset)

    def _get_length(self, index: int) -> int:
        """Get the length of the string at position `index` in the list."""
        start = self._get_offset(index)
        end = self._get_offset(index + 1)
        return end - start

    def append(self, value: str):
        """Append the string to the end of the list."""
        # NOTE: Remember that "offsets" stores the END of the string at
        # a given index. Therefore we must FIRST append the string and
        # THEN add the new length to "offsets"
        self.strings += struct.pack(
            f"{len(value)}s", value.encode(self.encoding)
        )
        self.offsets += struct.pack(self.size_type, len(self.strings))

    def copy(self) -> "StringList":
        """Create a deep copy of this list."""
        return StringList(self)

    def clear(self):
        """Remove all elements in this list."""
        self.offsets.clear()
        self.strings.clear()

    def count(self, value: str) -> int:
        """Get the sum of occurences of `value` in this list."""
        return sum(
            1 if s == value else 0
            for s
            in self
        )

    def extend(self, iterable: Iterable[str]):
        """Append all elements in `iterable` to this list."""
        for s in iterable:
            self.append(s)

    def index(self, value: str, start: Optional[int] = None, stop: Optional[int] = None) -> int:
        """Return the index of the first occurence of `value`."""
        if start is None:
            start = 0

        if stop is None:
            stop = len(self)

        index = next([s for s in self[start:stop] if s == value], None)

        if index is None:
            raise ValueError("Value is not contained in list.")

        return index

    def insert(self, index: int, value: str):
        """
        Insert `value` at `index` in the list.
        
        WARNING: Inserting elements near the beginning of the list may
        be slow since the internal offsets of all succeeding elements must be
        updated.
        """
        if index > len(self):
            self.append(value)
        else:
            # Insert the new offset/string value
            self.offsets[index:index] = struct.pack(index)
            string_offset = self._get_offset(index)
            self.strings[string_offset:string_offset] = struct.pack(value)

            # Update all offsets after the inserted value
            offset_delta = len(value)
            for i in range(index + 1, len(self)):
                self._set_offset(i, self._get_offset(i) + offset_delta)

    def pop(self, index: int = None) -> str:
        """
        Remove and return an element from the list.
        
        WARNING: Removing elements which are not near the end of the list may
        be slow since the internal offsets of all succeeding elements must be
        updated.
        """
        if index is None:
            index = len(self) - 1

        offset = self._get_offset(index)
        length = self._get_length(index)
        value = self._get_string(index)

        # Remove the given element from the offset/string buffer
        self.offsets = self.offsets[:index] + self.offsets[index + 1:]
        self.strings = self.strings[:offset] + self.strings[offset + length:]

        # Update all offsets after the removed value
        offset_delta = len(value)
        for i in range(index, len(self)):
            self._set_offset(i, self._get_offset(i) - offset_delta)

        return value

    def remove(self, element: str):
        """Remove the first occurence of `element` from the list."""
        index = self.index(element)
        self.pop(index)

    def reverse(self):
        """Reverse the order of all elements in the list."""
        reversed = StringList(size_type=self.size_type, encoding=self.encoding)
        for _ in range(len(self)):
            reversed.append(self.pop())

        self.offsets = reversed.offsets
        self.strings = reversed.strings
        
    def sort(self, key: Callable[[str], Any], reverse: bool):
        """Sort the list in ascending order."""
        _sorted = StringList(
            sorted(self, key=key, reverse=reverse),
            size_type=self.size_type,
            encoding=self.encoding,
        )

        self.offsets = _sorted.offsets
        self.strings = _sorted.strings
